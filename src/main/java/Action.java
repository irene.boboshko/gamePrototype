import lombok.extern.slf4j.Slf4j;
import races.*;
import troop.Troop;

import java.util.List;

@Slf4j
class Action {
    private TroopGenerator generator = new TroopGenerator();
    private Options options = new Options();
    private Troop player;

    private Race elves = new Elves();
    private Race orcs = new Orcs();
    private Race people = new People();
    private Race undeads = new Undeads();

    void actElvesTeam(Race race, List<Troop> team, List<Troop> rivalTeam) {
        System.out.print("==========================================================");
        player = generator.getCurrentPlayer(team);
        generator.getPrivileged().clear();
        outputAct(race, player);
        log.info("Attack of " + elves.getRace() + ": ");
        options.chooseElfOption(team, rivalTeam, player);
    }

    void actPeopleTeam(Race race, List<Troop> team, List<Troop> rivalTeam) {
        System.out.print("==========================================================");
        player = generator.getCurrentPlayer(team);
        generator.getPrivileged().clear();
        outputAct(race, player);
        log.info("Attack of " + people.getRace() + ": ");
        options.choosePeopleOption(team, rivalTeam, player);
    }

    void actOrcsTeam(Race race, List<Troop> team, List<Troop> rivalTeam) {
        player = generator.getCurrentPlayer(team);
        generator.getPrivileged().clear();
        outputAct(race, player);
        log.info("Attack of " + orcs.getRace() + ": ");
        options.chooseOrcOption(team, rivalTeam, player);
        System.out.println("\n==========================================================\n");
    }

    void actUndeadsTeam(Race race, List<Troop> team, List<Troop> rivalTeam) {
        player = generator.getCurrentPlayer(team);
        generator.getPrivileged().clear();
        outputAct(race, player);
        log.info("Attack of " + undeads.getRace() + ": ");
        options.chooseUndeadOption(rivalTeam, player);
        System.out.println("\n==========================================================\n");
    }

    private void outputAct(Race race, Troop player) {
        System.out.printf("\n%s%-8s%s%-12s%d%s%12s%5.0f%11s", "=== ", race, " --- ", player.getName(), player.getCurrentNumber(), ":" , player.getCurrentGroup(), player.getCurrentLife(), " === ");
    }
}

package races;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Elves implements Race {
    private String race = "Elves";

    @Override
    public String toString() {
        return race;
    }
}


package races;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class People implements Race {
    private String race = "People";

    @Override
    public String toString() {
        return race;
    }
}

package races;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Orcs implements Race {
    private String race = "Orcs";

    @Override
    public String toString() {
        return race;
    }
}

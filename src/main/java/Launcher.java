import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import races.*;
import troop.Troop;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@ToString
@Getter
@Slf4j
class Launcher {
    private TroopGenerator generator = new TroopGenerator();
    private List<Race> racesOfFirstTeam = new ArrayList<>();
    private List<Race> racesOfSecondTeam = new ArrayList<>();
    private Race chosenRaceOfFirstTeam;
    private Race chosenRaceOfSecondTeam;
    private List<Troop> firstTeam;
    private List<Troop> secondTeam;
    private Race chosenRace;
    private Action action = new Action();
    @Getter
    private ScheduledExecutorService executor;

    // the team of Elves/People
    private void startFirstTeam() {
        if (getChosenRaceOfFirstTeam() instanceof Elves) {
            action.actElvesTeam(getChosenRaceOfFirstTeam(), getFirstTeam(), getSecondTeam());
        } else if (getChosenRaceOfFirstTeam() instanceof People) {
            action.actPeopleTeam(getChosenRaceOfFirstTeam(), getFirstTeam(), getSecondTeam());
        }
    }

    // the team of Orcs/Undeads
    private void startSecondTeam() {
        if (getChosenRaceOfSecondTeam() instanceof Orcs) {
            action.actOrcsTeam(getChosenRaceOfSecondTeam(), getSecondTeam(), getFirstTeam());
        } else if (getChosenRaceOfSecondTeam() instanceof Undeads) {
            action.actUndeadsTeam(getChosenRaceOfSecondTeam(), getSecondTeam(), getFirstTeam());
        }
    }

    void startExecution() {
        executor = Executors.newScheduledThreadPool(2);

        Runnable task = () -> {
            try {
                TimeUnit.MILLISECONDS.sleep(500);
                    startFirstTeam();
                if (getSecondTeam().isEmpty()) {
                    System.out.println("\n");
                    finish();
                }
                startSecondTeam();
                if (getFirstTeam().isEmpty()) {
                        finish();
                    }
            } catch (InterruptedException e) {
                System.err.println("Task interrupted");
                shutdownAndAwaitTermination();
            }
        };
        executor.scheduleWithFixedDelay(task, 1000, 500, TimeUnit.MILLISECONDS);
    }

    private void shutdownAndAwaitTermination() {
        try {
            log.info("Attempt to shutdown executor");
            executor.shutdown();
            executor.awaitTermination(5, TimeUnit.SECONDS);
            log.info("Shutdown finished: " + String.valueOf(executor.isShutdown()));
        }
        catch (InterruptedException e) {
            log.error("Tasks interrupted: " + e);
        }
        finally {
            if (!executor.isTerminated() && !executor.isShutdown()) {
                log.info("Cancelling non-finished tasks");
            }
            executor.shutdownNow();
            log.info("Repeated check for being finished: " + String.valueOf(executor.isShutdown()));
        }
    }

    void createRaces() {
        racesOfFirstTeam.add(new Elves());
        racesOfFirstTeam.add(new People());

        racesOfSecondTeam.add(new Orcs());
        racesOfSecondTeam.add(new Undeads());
        getChosenRaces();
    }

    private void getChosenRaces() {
        firstTeam = getList(racesOfFirstTeam);
        this.chosenRaceOfFirstTeam = chosenRace;

        secondTeam = getList(racesOfSecondTeam);
        this.chosenRaceOfSecondTeam = chosenRace;
}

    private List<Troop> getList(List<Race> races) {
        chosenRace = generator.getRandomRace(races);
        return generator.generateTroop(chosenRace);
    }

    private void finish() {
        if (firstTeam.isEmpty()) {
            finishGame(getChosenRaceOfFirstTeam(), getChosenRaceOfSecondTeam());
            shutdownAndAwaitTermination();
        }
        if (secondTeam.isEmpty()) {
            finishGame(getChosenRaceOfSecondTeam(), getChosenRaceOfFirstTeam());
            shutdownAndAwaitTermination();
        }
    }

    private void finishGame(Race raceOne, Race raceTwo) {
        String finish = "\nThe team of " + raceOne + " has no more players.\n" + "The " + raceTwo + " have won.\n" + "Game over\n";
        System.out.println(finish);
        log.info(finish);
    }
}
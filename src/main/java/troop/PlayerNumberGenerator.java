package troop;

import lombok.Getter;

@Getter
public class PlayerNumberGenerator {
    private int currentNumber;
    private final static int LIST_SIZE = 8;

    public PlayerNumberGenerator() {
        currentNumber = 1;
    }

    public static int getListSize() {
        return LIST_SIZE;
    }

    public int getNextNumber() {
        if (currentNumber > getListSize()) {
            currentNumber = 1;
        }
        return currentNumber++;
    }
}

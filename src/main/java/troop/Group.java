package troop;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static troop.Troop.controller;

@Getter
@Slf4j
public class Group {
    private String ordinary = "Ordinary";
    private String privileged = "Privileged";
    private String group;

    public boolean isOrdinary(Troop player) {
        return player.getCurrentGroup().equals(ordinary);
    }

    public String getOrdinaryStatus() {
        this.group = ordinary;
        return group;
    }

    private void includeIntoPrivileged(Troop player) {
        this.group = privileged;
        player.setCurrentGroup(group);
    }

    public void excludeFromPrivileged(Troop player) {
        if (!isOrdinary(player)) {
            this.group = ordinary;
            player.setCurrentGroup(group);
        }
    }

    public String actForOrdinaryOrPrivileged(List<Troop> rivalTeam, Troop rival, double loss, String complete) {
        if (isOrdinary(rival)) {
            return actForOrdinary(rivalTeam, rival, loss, complete);
        } else {
            return actForPrivileged(rivalTeam, rival, loss, complete);
        }
    }

    private String actForPrivileged(List<Troop> rivalTeam, Troop rival, double loss, String complete) {
        loss *= 1.5;
        excludeFromPrivileged(rival);
        controller.changeLifeSize(rivalTeam, rival, loss);
        if (!controller.isAlive(rival)) {
            return complete + ". Current life level is " + String.format("%.0f",rival.getCurrentLife()) + "." + controller.killed();
        } else {
            return complete + ". Current life level is " + String.format("%.0f",rival.getCurrentLife()) + ".";
        }
    }

    private String actForOrdinary(List<Troop> rivalTeam, Troop rival, double loss, String complete) {
        controller.changeLifeSize(rivalTeam, rival, loss);
        if (!controller.isAlive(rival)) {
            return complete + ". Current life level is " + String.format("%.0f",rival.getCurrentLife()) + "." + controller.killed();
        } else {
            return complete + ". Current life level is " + String.format("%.0f",rival.getCurrentLife()) + ".";
        }
    }

    String actForOrdinaryOrPrivileged(Troop player, String completeForOrdinary, String completeForPrivileged) {
        if (isOrdinary(player)) {
            includeIntoPrivileged(player);
            return completeForOrdinary;
        } else {
            return completeForPrivileged;
        }
    }

    @Override
    public String toString() {
        return group;
    }
}

package troop.players;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import races.Orcs;
import races.Race;
import races.Undeads;
import troop.Troop;

import java.util.List;

@ToString
@Getter
@Setter
@Slf4j
public class Magician implements Troop {
    private String name = "Magician";
    private int currentNumber;
    private String currentGroup;
    private double currentLife;

    public Magician() {
        currentNumber = number.getNextNumber();
        currentGroup = group.getOrdinaryStatus();
        currentLife = controller.getCurrentLife();
    }

    public String getName() {
        return name;
    }

    @Override
    public String setName(Race race) {
        if (race instanceof Orcs) {
            return this.name = "Shaman";
        } else if (race instanceof Undeads) {
            return this.name = "Necromancer";
        } else {
            return name;
        }
    }

    public String castSpell(Troop rival) {
        if (rival.getCurrentGroup().equals(group.getPrivileged())) {
            group.excludeFromPrivileged(rival);
            return rival.getName() + " (#" + rival.getCurrentNumber() + ")" + " has been excluded from the Privileged group.";
        } else {
            return "Casting the spell... Ooops, no privileged character was found.";
        }
    }

    public String makeIll(List<Troop> rivalTeam, Troop rival) {
        group.excludeFromPrivileged(rival);
        controller.changeLifeSize(rivalTeam, rival);
        String complete = rival.getName() + " (#" + rival.getCurrentNumber() + ")" + " is ill... Current life level is " + String.format("%.0f",rival.getCurrentLife());
        controller.isAlive(rival);
        return complete;
    }
}
package troop;

import lombok.Getter;

import java.util.List;

@Getter
public class LifeController {
    private double currentLife;
    private final static double LIFE_SIZE = 100;
    Group group = new Group();

    public LifeController() {
        currentLife = LIFE_SIZE;
    }

    void changeLifeSize(List<Troop> troop, Troop player, double lifeChangeIndex) {
        currentLife = player.getCurrentLife();
        currentLife += lifeChangeIndex;
        player.setCurrentLife(currentLife);
        removeIfNotAlive(troop, player);
    }

    //reducing the loss by 50%
    public void changeLifeSize(List<Troop> troop, Troop player) {
        currentLife = player.getCurrentLife();
        currentLife /= 2;
        player.setCurrentLife(currentLife);
        removeIfNotAlive(troop, player);
    }

    public boolean isAlive(Troop player) {
        return player.getCurrentLife() > 0;
    }

    String killed() {
        return " The player has been killed.";
    }

    private void removeIfNotAlive(List<Troop> troop, Troop player) {
        if (!isAlive(player)) {
            troop.remove(player);
        }
    }
}

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import races.Race;
import troop.Group;
import troop.LifeController;
import troop.PlayerNumberGenerator;
import troop.Troop;
import troop.players.Fighter;
import troop.players.Magician;
import troop.players.Shooter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Getter
@Slf4j
class TroopGenerator {
    private List<Troop> troop;
    private PlayerNumberGenerator number = new PlayerNumberGenerator();
    private LifeController controller = new LifeController();
    private Group group = new Group();
    private Random random = new Random();
    @Getter
    private List<Troop> privileged;

    List<Troop> generateTroop(Race race) {
        log.info("Beginning to create the team of " + race + ".");
        troop = new ArrayList<>(PlayerNumberGenerator.getListSize());
        troop.add(new Magician());
        troop.add(new Shooter());
        troop.add(new Shooter());
        troop.add(new Shooter());
        troop.add(new Fighter());
        troop.add(new Fighter());
        troop.add(new Fighter());
        troop.add(new Fighter());
        output(race);
        log.info("The team of " + race + " has been created.");
        return troop;
    }

   Race getRandomRace(List<? extends Race> list) {
        return list.get(random.nextInt(list.size()));
    }

    <T> T getRandom(List<T> list) {
        return list.get(random.nextInt(list.size()));
    }

    Troop getCurrentPlayer(List<Troop> team) {
        selectPrivileged(team);
        return getPrivileged().isEmpty() ? getRandom(team) : getRandom(getPrivileged());
    }

    private void selectPrivileged(List<Troop> team) {
        privileged = team
                .stream()
                .filter(player -> !group
                        .isOrdinary(player))
                .collect(Collectors.toList());
    }

    private void output(Race race) {
        System.out.println("==========================================================");
        System.out.printf("%s%s%10s\n", "= The troop of ", race, " has been created.");
        System.out.println("==========================================================");
        System.out.printf("%s%27s%15s%12s\n", "RACE", "ROLE", "GROUP", "LIFE");

        for (Troop player : troop) {
            log.info(race + ": --- PLAYER " + player.getCurrentNumber() + " " + player.setName(race) + "   " + player.getCurrentGroup() + "   " + String.format("%.0f",player.getCurrentLife()));
            System.out.printf("%-8s%s%d%12s%15s%10.0f\n", race, " --- PLAYER ", player.getCurrentNumber(), player.setName(race), player.getCurrentGroup(), player.getCurrentLife());
        }
        System.out.println("==========================================================\n");
    }
}

package weapon;

import lombok.Getter;

@Getter
public class Arbalest implements Weapon {
    private String weapon = "arbalest";

    @Override
    public String getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return weapon;
    }
}

package weapon;

import lombok.Getter;

@Getter
public class Blade implements Weapon {
    private String weapon = "blade";

    @Override
    public String getWeapon() {
        return weapon;
    }

    @Override
    public String toString() {
        return weapon;
    }
}

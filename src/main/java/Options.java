import lombok.extern.slf4j.Slf4j;
import troop.Group;
import troop.Troop;
import troop.players.Fighter;
import troop.players.Magician;
import troop.players.Shooter;
import weapon.*;

import java.util.ArrayList;
import java.util.List;

@Slf4j
class Options {
    private TroopGenerator generator = new TroopGenerator();
    private List<String> actions = new ArrayList<>();
    private Group group = new Group();
    private Magician magician = new Magician();
    private Fighter fighter = new Fighter();
    private Shooter shooter = new Shooter();

    private Weapon arbalest = new Arbalest();
    private Weapon blade = new Blade();
    private Weapon bow = new Bow();
    private Weapon club = new Club();
    private Weapon spear = new Spear();
    private Weapon sword = new Sword();

    void chooseElfOption(List<Troop> team, List<Troop> rivalTeam, Troop player) {
        if (player.getName().equals("Magician")) {
            actions.add(magician.improve(generator.getCurrentPlayer(team)));
            actions.add(magician.reduce(rivalTeam, generator.getRandom(rivalTeam), -10));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Archer")) {
            actions.add(shooter.shoot(rivalTeam, bow.getWeapon(), -7, generator.getRandom(rivalTeam)));
            actions.add(shooter.attack(rivalTeam, -3, generator.getRandom(rivalTeam)));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Fighter")) {
            String action = fighter.attack(rivalTeam, sword.getWeapon(), -15, generator.getRandom(rivalTeam));
            act(action);
            group.excludeFromPrivileged(player);
        }
    }

    void choosePeopleOption(List<Troop> team, List<Troop> rivalTeam, Troop player) {
        if (player.getName().equals("Magician")) {
            actions.add(magician.improve(generator.getRandom(team)));
            actions.add(magician.reduce(rivalTeam, generator.getRandom(rivalTeam), -4));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Arbalester")) {
            actions.add(shooter.shoot(rivalTeam, arbalest.getWeapon(), -5, generator.getRandom(rivalTeam)));
            actions.add(shooter.attack(rivalTeam, -3, generator.getRandom(rivalTeam)));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Fighter")) {
            String action = fighter.attack(rivalTeam, sword.getWeapon(), -18, generator.getRandom(rivalTeam));
            act(action);
            group.excludeFromPrivileged(player);
        }
    }

    void chooseOrcOption(List<Troop> team, List<Troop> rivalTeam, Troop player) {
        if (player.getName().equals("Shaman")) {
            actions.add(magician.improve(generator.getRandom(team)));
            actions.add(magician.castSpell(generator.getCurrentPlayer(rivalTeam)));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Archer")) {
            actions.add(shooter.shoot(rivalTeam, bow.getWeapon(), -3, generator.getRandom(rivalTeam)));
            actions.add(shooter.attack(rivalTeam, blade.getWeapon(), -2, generator.getRandom(rivalTeam)));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Goblin")) {
            String action = fighter.attack(rivalTeam, club.getWeapon(), -20, generator.getRandom(rivalTeam));
            act(action);
            group.excludeFromPrivileged(player);
        }
    }

    void chooseUndeadOption(List<Troop> rivalTeam, Troop player) {
        if (player.getName().equals("Necromancer")) {
            actions.add(magician.makeIll(rivalTeam, generator.getRandom(rivalTeam)));
            actions.add(magician.reduce(rivalTeam, generator.getRandom(rivalTeam), -5));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Hunter")) {
            actions.add(shooter.shoot(rivalTeam, bow.getWeapon(), -4, generator.getRandom(rivalTeam)));
            actions.add(shooter.attack(rivalTeam, -2, generator.getRandom(rivalTeam)));
            act(actions);
            group.excludeFromPrivileged(player);
        }
        if (player.getName().equals("Zombie")) {
            String action = fighter.attack(rivalTeam, spear.getWeapon(), -18, generator.getRandom(rivalTeam));
            act(action);
            group.excludeFromPrivileged(player);
        }
    }

    private void act(List<String> actions) {
        startActionsOutput();
        String random = generator.getRandom(actions);
        log.info(random);
        System.out.print(random);
        actions.clear();
    }

    private void act(String action) {
        startActionsOutput();
        log.info(action);
        System.out.print(action);
    }
    private void startActionsOutput() {
        System.out.printf("%12s", "<<<<< ");
    }
}
